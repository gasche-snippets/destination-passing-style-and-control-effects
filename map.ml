(* ocamlfind ocamlopt -inline 5 -package core_bench -thread -linkpkg -o test bench_map.ml *)

let init n =
  let rec init acc = function
  | 0 -> acc
  | n -> init (n :: acc) (n - 1)
  in init [] n

let rec map_nontail f = function
  | [] -> []
  | x :: xs ->
     let a = f x in a :: map_nontail f xs

let map_rev f xs =
  let rec rev_map f acc = function
    | [] -> acc
    | x :: xs -> rev_map f (f x :: acc) xs in
  let rec rev acc = function
    | [] -> acc
    | x :: xs -> rev (x :: acc) xs in
  rev_map f [] (rev [] xs)

(* Thanks to Jacques Garrigue for suggesting the following structure *)
type 'a mut_list = {
  hd: 'a;
  mutable tl: 'a list
}
external inj : 'a mut_list -> 'a list = "%identity"

module Acc = struct
  
  let create x = { hd = x; tl = [] }

  let accum acc x =
    let cell = { hd = x; tl = [] } in
    acc.tl <- inj cell;
    cell

  let return head = inj head
end

let map_dst f = function
  | [] -> []
  | x :: xs ->
     let open Acc1 in
     let rec loop f acc = function
       | [] -> return acc
       | x :: xs ->
          loop f (accum acc (f x)) xs
     in
     let acc = create (f x) in
     loop f acc xs

module Acc2 = struct
  let rec canary = Obj.magic () :: canary
  
  let copy head limit =
    let rec copy li limit tail =
      match li with
        | [] -> assert false
        | hd::tl ->
           let cell = { hd; tl = canary } in
           tail.tl <- inj cell;
           if li == limit then cell
           else copy tl limit cell
    in
    let new_head = { hd = head.hd; tl = canary } in
    if inj head == limit then (new_head, new_head)
    else begin
      let tail = copy head.tl limit new_head in
      new_head, tail
    end

  let create x = { hd = x; tl = canary }

  let fresh acc = acc.tl == canary

  let accum acc x =
    let cell = { hd = x; tl = canary } in
    acc.tl <- inj cell;
    cell

  let return head acc =
    acc.tl <- [];
    inj head
end

let rec map_dst2 f = function
  | [] -> []
  | x :: xs ->
     let open Acc2 in
     (* precondition: acc is fresh *)
     let rec loop f head acc = function
       | [] -> return head acc
       | x :: xs ->
	  let next = f x in
	  (* control effects during (f x) may destroy freshness *)
	  if fresh acc then loop f head (accum acc next) xs
	  else begin
	     let head, acc = copy head (inj acc) in
	     (* fresh after copy *)
	     loop f head (accum acc next) xs
	  end
     in
     let first = f x in
     let head = create first in
     (* head is fresh *)
     loop f head head xs

(*
(* If you want to benchmark *)
open Core.Std
open Core_bench.Std

let test n =
  Printf.printf "Run with lists of size %d\n%!" n;
  let li = init n in
  Command.run (Bench.make_command [
    Bench.Test.create ~name:"map_nontail"
      (fun len -> ignore (map_nontail succ li));
    Bench.Test.create ~name:"map_rev"
      (fun len -> ignore (map_rev succ li));
    Bench.Test.create ~name:"map_dst"
      (fun len -> ignore (map_dst succ li));
    Bench.Test.create ~name:"map_dst2"
      (fun len -> ignore (map_dst2 succ li));
    Bench.Test.create ~name:"Core.map"
      (fun len -> ignore (List.map ~f:succ li));
  ])

let () = test 5; test 1000; test 100_000
 *)


(* (* If you want to test HANSEI, from the hansei source directory *)
#use "topfind";;
#require "delimcc";;
#load "prob.cma";;

open ProbM

let test = exact_reify (fun () -> flip 0.5);;
let check map =
  let maybe_negate n = if flip 0.5 then n else -n in
  exact_reify (fun () -> map maybe_negate [1;2;3])
*)
